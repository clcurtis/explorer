<?php
 require_once('./config.php');
?>
<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <link rel="stylesheet" href="./style.css" type="text/css" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dygraph/2.1.0/dygraph.css" type="text/css" />
  <link rel="shortcut icon" href="./favicon.ico" type="image/x-icon">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/dygraph/2.1.0/dygraph.min.js"></script>
  <title>Boliecoin block explorer</title>
 </head>
 <body>
 <div id="menuback">
  <div id="menubar">
   <div id="logotext"> Boliecoin block explorer</a></div>
   <div id="searchbox"><form action="./?page=search" method="get"><input type="hidden" name="page" value="search" /><input type="text" name="id" /> <input type="submit" value="Search" /></form></div>
   <div id="menu">
    <a href="./">Home</a> 
   </div>
  </div>
 </div>
 <div id="content">
  <?php
   $page = isset($_GET['page']) ? $_GET['page'] : '';
   switch ($page) {
    case 'contact':
     require_once('contact.php');
     break;
    case 'block':
     require_once('block.php');
     break;
    case 'search':
     require_once('search.php');
     break;
    case 'tx':
     require_once('tx.php');
     break;
    default:
     require_once('blocklist.php');
     break;
   }
  ?>
 </div>
 <div id="footerback">
 </div>
</html>
 </body>
</html>
